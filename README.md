# recommender_system
2019-12-03	Project Presentation Date  
2019-11-28	Recommendation System  
2019-11-14	Map-reduce Algorithm  
2019-11-07	Database ready, Platform setup  
2019-11-05	Project Proposal  

AWS EMR setup:
s3 : 
fileList.txt
install.sh
preprocess_mapper.py
preprocess_reducer.py

when create cluster:
add bootstrap action to run install.sh which installs python packages
