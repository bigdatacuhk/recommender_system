::@echo off

:: Commit Message Edit
set /p msg="Commit Message: "
pause

:: Stage all changes
git add -A

:: Commit Git Respository
git commit -a -m "%msg%"
:: -a <include all the changed files>

:: Force Push to Master of Git Respository
git push --force

pause