#!/usr/bin/python
from operator import itemgetter
import sys

for line in sys.stdin:
    line = line.strip()
    customer_id, product_id_star_rating = line.split('\t', 1)
    product_id, star_rating = product_id_star_rating.split(',')
    print('%s\t%s\t%s' % (customer_id, product_id, star_rating))
