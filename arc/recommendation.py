import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import pairwise_distances
from os import walk
import os

USER_ID = 'userId'
ITEM_ID = 'itemId'
RATING = 'rating'
FILE_DIR = 'data'

def getFileName(fileDir):
    f = []
    for (dirpath, dirnames, filenames) in walk(fileDir):
        for filename in filenames:
            if filename != '.DS_Store':
                f.append(os.path.join(dirpath, filename)) 
    return f

def readData(files, columnName):
    dfs = []
    for file in files:
        dfs.append(pd.read_csv(file, sep="\t", header=None))

    df = pd.concat(dfs, ignore_index=True)
    df.columns = columnName
    return df

      
def setTable(ratings):
    m = ratings.groupby(by=USER_ID, as_index=False)[RATING].mean()
    avg = pd.merge(ratings, m, on=USER_ID)
    avg['cenRating'] = avg['rating_x'] - avg['rating_y']
    
    user_item = pd.pivot_table(avg, values='rating_x',index=USER_ID,columns=ITEM_ID)
    user_item_cen = pd.pivot_table(avg, values='cenRating',index=USER_ID,columns=ITEM_ID)
    user_item_avgItem = user_item_cen.fillna(user_item_cen.mean(axis=0))        
    avg = avg.astype({ITEM_ID: str})
    item_user = avg.groupby(by = USER_ID)[ITEM_ID].apply(lambda x:','.join(x))

    return m, user_item, user_item_avgItem, item_user


def cosSim(user_item):
    mat = cosine_similarity(user_item)
    np.fill_diagonal(mat, 0 )
    sim = pd.DataFrame(mat,index=user_item.index)
    sim.columns=user_item.index
    return sim
    

def topNNeighbours(df,n):
    order = np.argsort(df.values, axis=1)[:, :n]
    df = df.apply(lambda x: pd.Series(x.sort_values(ascending=False).iloc[:n].index, 
          index=['top{}'.format(i) for i in range(1, n+1)]), axis=1)
    return df

ratings = readData(getFileName(FILE_DIR), [USER_ID, ITEM_ID, RATING])
mean, user_item, user_item_avgItem, item_user = setTable(ratings)
similarityByItem = cosSim(user_item_avgItem)
sim_user_30_m = topNNeighbours(similarityByItem, 30)

def predictItemScores(user):
    Item_bought_by_user = user_item.columns[user_item[user_item.index==user].notna().any()].tolist()
    a = sim_user_30_m[sim_user_30_m.index==user].values
    b = a.squeeze().tolist()
    d = item_user[item_user.index.isin(b)]
    l = ','.join(d.values)
    Item_bought_by_similar_users = l.split(',')
    Items_under_consideration = list(set(Item_bought_by_similar_users)-set(list(map(str, Item_bought_by_user))))
    Items_under_consideration = list(map(int, Items_under_consideration))
    score = []
    for item in Items_under_consideration:
        c = user_item_avgItem.loc[:,item]
        d = c[c.index.isin(b)]
        f = d[d.notnull()]
        avg_user = mean.loc[mean[USER_ID] == user,RATING].values[0]
        index = f.index.values.squeeze().tolist()
        corr = similarityByItem.loc[user,index]
        fin = pd.concat([f, corr], axis=1)
        fin.columns = ['adg_score','correlation']
        fin['score']=fin.apply(lambda x:x['adg_score'] * x['correlation'],axis=1)
        nume = fin['score'].sum()
        deno = fin['correlation'].sum()
        final_score = avg_user + (nume/deno)
        score.append(final_score)
    data = pd.DataFrame({ITEM_ID:Items_under_consideration,'score':score})
    
    return data

def topNRecommendation(data, n):
    n = n if n <= len(data) else len(data)
    recommendations = data.sort_values(by='score',ascending=False).head(n)
    return recommendations
    

while True:
    entry = input("Enter the User ID that you want to recommend the items : ")
    if not entry.isnumeric():
        break
    user = int(entry)
    predictedScores = predictItemScores(user)
    print("The Recommendations for User ID - " + str(user) + ":")
    nRec = topNRecommendation(predictedScores, 20)
    print(nRec)
    print("\n")
        