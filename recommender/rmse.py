########################################################################################################################################
# The following code are modified from:
# https://medium.com/sfu-big-data/recommendation-systems-user-based-collaborative-filtering-using-n-nearest-neighbors-bf7361dc24e0
# https://github.com/ashaypathak/Recommendation-system
########################################################################################################################################

import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import pairwise_distances
from sklearn.metrics import mean_squared_error
from math import sqrt
from os import walk
import os
import json

USER_ID = 'userId'
ITEM_ID = 'itemId'
RATING = 'rating'
FILE_DIR = 'data'
TOP_N = 30
RATIO = 0.8

def getFileName(fileDir):
    f = []
    for (dirpath, dirnames, filenames) in walk(fileDir):
        for filename in filenames:
            if filename != '.DS_Store':
                f.append(os.path.join(dirpath, filename)) 
    return f

def readData(files, columnName):
    dfs = []
    for file in files:
        dfs.append(pd.read_csv(file, sep="\t", header=None))

    df = pd.concat(dfs, ignore_index=True)
    df.columns = columnName
    return df

      
def setTable(ratings):
    m = ratings.groupby(by=USER_ID, as_index=False)[RATING].mean()
    avg = pd.merge(ratings, m, on=USER_ID)
    avg['cenRating'] = avg['rating_x'] - avg['rating_y']
    
    user_item = pd.pivot_table(avg, values='rating_x',index=USER_ID,columns=ITEM_ID)
    user_item_cen = pd.pivot_table(avg, values='cenRating',index=USER_ID,columns=ITEM_ID)
    user_item_avgItem = user_item_cen.fillna(user_item_cen.mean(axis=0))        
    avg = avg.astype({ITEM_ID: str})
    item_user = avg.groupby(by = USER_ID)[ITEM_ID].apply(lambda x:','.join(x))

    return m, user_item, user_item_avgItem, item_user


def cosSim(user_item):
    mat = cosine_similarity(user_item)
    np.fill_diagonal(mat, 0 )
    sim = pd.DataFrame(mat,index=user_item.index)
    sim.columns=user_item.index
    return sim
    

def topNNeighbours(df,n):
    order = np.argsort(df.values, axis=1)[:, :n]
    df = df.apply(lambda x: pd.Series(x.sort_values(ascending=False).iloc[:n].index, 
          index=['top{}'.format(i) for i in range(1, n+1)]), axis=1)
    return df

def predictItemScores(user, user_item, userTopNNB, item_user, user_item_avgItem, mean, similarityByItem):
    userItemBought = user_item.columns[user_item[user_item.index==user].notna().any()].tolist()
    a = userTopNNB[userTopNNB.index==user].values
    b = a.squeeze().tolist()
    d = item_user 

    l = ','.join(d.values)
    simUserItemBought = l.split(',')
    itemConsiderList = list(set(simUserItemBought)-set(list(map(str, userItemBought))))
    itemConsiderList = list(map(str, itemConsiderList))
    score = []
    for item in itemConsiderList:
        c = user_item_avgItem.loc[:,item]
        d = c[c.index.isin(b)]
        f = d[d.notnull()]
        avg_user = mean.loc[mean[USER_ID] == user,RATING].values[0]
        index = f.index.values.squeeze().tolist()
        corr = similarityByItem.loc[user,index]
        fin = pd.concat([f, corr], axis=1)
        fin.columns = ['adg_score','correlation']
        fin['score']=fin.apply(lambda x:x['adg_score'] * x['correlation'],axis=1)
        nume = fin['score'].sum()
        deno = fin['correlation'].sum()
        final_score = avg_user + (nume/deno)
        score.append(final_score)
    data = pd.DataFrame({ITEM_ID:itemConsiderList,'score':score})
    
    return data

def topNRecommendation(data, n):
    n = n if n <= len(data) else len(data)
    recommendations = data.sort_values(by='score',ascending=False).head(n)
    return recommendations
    
ratings = readData(getFileName(FILE_DIR), [USER_ID, ITEM_ID, RATING])

train = np.random.rand(len(ratings)) < RATIO
test = ~train

trainData = ratings[train]
testData = ratings[test]


mean, user_item, user_item_avgItem, item_user = setTable(trainData)
similarityByItem = cosSim(user_item_avgItem)
userTopNNB = topNNeighbours(similarityByItem, TOP_N)

r_true = []
r_pred = []

userList = pd.unique(testData[USER_ID])

for user in userList:
        
    userTestList = testData.loc[testData[USER_ID]==user]

    predictedScores = predictItemScores(user, user_item, userTopNNB, item_user, user_item_avgItem, mean, similarityByItem)
    
    for index, row in userTestList.iterrows():
        s = predictedScores.loc[predictedScores[ITEM_ID]==str(row[1])]['score']
        
        if not s.empty:
            r_true.append(row[2])
            r_pred.append(s.values[0])
    
RMSE = sqrt(mean_squared_error(r_true, r_pred))
print("RMSE: " , round(RMSE, 6))
        